import { AuthRequestRepository } from './../repositories/auth-request-repository/auth-request-repository';
import { PartnerRepository } from './../repositories/partner-repository/partner-repository';
import { VerificationRepository } from './../repositories/verification-repository/verification-repository';
import { TokenRepository } from './../repositories/token-repository/token-repository';
import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';


@Module({
    imports: [DbModule],
    providers: [TokenRepository, VerificationRepository, PartnerRepository,
    AuthRequestRepository],
    exports: [TokenRepository, VerificationRepository, PartnerRepository, AuthRequestRepository],
})
export class AuthRepositoryModule {}
