import { VerificationCode } from '@neb-sports/mysamay-authorization-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery } from 'mongodb';



@Injectable()
export class VerificationRepository {
    db: Db
    collection: Collection<VerificationCode>;

    constructor(private dbProvider: MongoProvider) {
    }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(VerificationCode.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createVerificationCode(verificationCode: VerificationCode): Promise<VerificationCode> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(verificationCode);
            if(result.insertedCount == 1) {
                return result.ops[0];
            }
            return undefined;
        } catch(error) {
            Promise.reject(error);
        }
    }

    async getVerificationCodeByAnyQuery(query: FilterQuery<VerificationCode>): Promise<VerificationCode> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query);
            if(result) {
                return result;
            }
            return undefined;
        } catch(error) {
            Promise.reject(error);
        }
    }

    async deleteByAnyQuery(query: FilterQuery<VerificationCode>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }
    
}
