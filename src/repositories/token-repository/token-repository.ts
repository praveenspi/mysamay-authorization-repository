import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { AuthToken } from "@neb-sports/mysamay-authorization-model";

@Injectable()
export class TokenRepository {
    db: Db;
    collection: Collection<AuthToken>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(AuthToken.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createAuthToken(authToken: AuthToken): Promise<AuthToken> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(authToken);
            if(result.insertedCount === 1) {
                return result.ops[0];
            }
            return undefined;
        }
        catch (error) {
            return Promise.reject(error);
        }
    }

    async removeAuthToken(tokenHash: string): Promise<boolean> {
        try {
            await this.getCollection();
            let result = await this.collection.deleteOne({tokenHash: tokenHash});
            if(result.deletedCount === 1) {
                return true;
            }
            return false;
        }
        catch (error) {
            return Promise.reject(error);
        }
    }

    async getAuthToken(tokenHash: string): Promise<AuthToken> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne({tokenHash: tokenHash});
            if(result) {
                return result;
            }
            return undefined;
        }
        catch (error) {
            return Promise.reject(error);
        }
    }

    async deleteByAnyQuery(query: FilterQuery<AuthToken>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }
}
