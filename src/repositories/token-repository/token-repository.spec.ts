import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';







import { TokenRepository } from "./token-repository";

describe('EventsRepository', () => {
    let provider: TokenRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [TokenRepository],
        }).compile();

        provider = module.get<TokenRepository>(TokenRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
