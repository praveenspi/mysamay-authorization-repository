import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { AuthRequest } from "@neb-sports/mysamay-authorization-model";

@Injectable()
export class AuthRequestRepository {
    db: Db;
    collection: Collection<AuthRequest>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(AuthRequest.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createAuthRequest(authRequest: AuthRequest): Promise<AuthRequest> {
        await this.getCollection();
        let result = await this.collection.insertOne(authRequest);
        if (result.insertedCount === 1) {
            return result.ops[0];
        }
        return undefined;
    }

    async updateAuthRequest(authRequest: AuthRequest): Promise<AuthRequest> {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: authRequest._id },
            authRequest,
            { returnOriginal: false },
        );
        if (result.ok === 1) return result.value;
        else return undefined;
    }

    async getAuthReqByAnyQuery(
        query: FilterQuery<AuthRequest>,
        projection: SchemaMember<AuthRequest, any>,
    ): Promise<AuthRequest> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
            projection: projection,
            sort: {createdTime: -1}
        });
        if (result) return result;
        else return undefined;
    }

    async countAuthReqByAnyQuery(
        query: FilterQuery<AuthRequest>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async deleteAuthReqByAnyQuery(query: FilterQuery<AuthRequest>) {
        await this.getCollection();
        let result = await this.collection.deleteMany(query);
        if(result.deletedCount > 0) {
            return true;
        }
        return false;
    }

    async updateMany(query: FilterQuery<AuthRequest>, data: Partial<AuthRequest>) {
        await this.getCollection();
        await this.collection.updateMany(query, {$set: data})
    }

}
