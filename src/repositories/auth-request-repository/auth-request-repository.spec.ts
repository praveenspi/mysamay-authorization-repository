import { AuthRequestRepository } from './auth-request-repository';
import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';

describe('EventsRepository', () => {
    let provider: AuthRequestRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [AuthRequestRepository],
        }).compile();

        provider = module.get<AuthRequestRepository>(AuthRequestRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
