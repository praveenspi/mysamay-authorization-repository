import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { Partner } from "@neb-sports/mysamay-authorization-model";

@Injectable()
export class PartnerRepository {
    db: Db;
    collection: Collection<Partner>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(Partner.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createPartner(partner: Partner): Promise<Partner> {
        await this.getCollection();
        let result = await this.collection.insertOne(partner);
        if (result.insertedCount === 1) {
            return result.ops[0];
        }
        return undefined;
    }

    async updatePartner(partner: Partner): Promise<Partner> {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: partner._id },
            partner,
            { returnOriginal: false },
        );
        if (result.ok === 1) return result.value;
        else return undefined;
    }

    async getPartnerByAnyQuery(
        query: FilterQuery<Partner>,
        projection: SchemaMember<Partner, any>,
    ): Promise<Partner> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
            projection: projection,
        });
        if (result) return result;
        else return undefined;
    }

    async countPartnerByAnyQuery(
        query: FilterQuery<Partner>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getPartnerByAnyQueryPaginated(
        query: FilterQuery<Partner>,
        skip: number,
        limit: number,
        sort: SortOptionObject<Partner>,
        projection: SchemaMember<Partner, any>,
    ): Promise<Partner[]> {
        await this.getCollection();
        let result = await this.collection
            .find(query, { projection: projection })
            .skip(skip)
            .limit(limit)
            .sort(sort)
            .toArray();
        if (result && result.length > 0) return result;
        else return undefined;
    }

    async deletePartnerByAnyQuery(query: FilterQuery<Partner>) {
        await this.getCollection();
        let result = await this.collection.deleteMany(query);
        if(result.deletedCount > 0) {
            return true;
        }
        return false;
    }

}
