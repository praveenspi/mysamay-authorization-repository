import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';
import { PartnerRepository } from "./partner-repository";

describe('EventsRepository', () => {
    let provider: PartnerRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [PartnerRepository],
        }).compile();

        provider = module.get<PartnerRepository>(PartnerRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
